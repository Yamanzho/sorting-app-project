import org.epam.test.SortingApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppParametrizedTest {
    private final int[] input;
    private final int[] expected;

    SortingApp sorting = new SortingApp();

    public SortingAppParametrizedTest(int[] input, int[] expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{2, 3}, new int[]{2, 3}},
                {new int[]{0, 0}, new int[]{0, 0}},
                {new int[]{10, 5}, new int[]{5, 10}},
                {new int[]{-5, 5}, new int[]{-5, 5}},
                {new int[]{5}, new int[]{5}},
                {new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
                {new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}},
        });
    }

    @Test
    public void testSort() {
        sorting.sort(input);
        assertArrayEquals(expected, input);
    }
}
