import org.epam.test.SortingApp;
import org.junit.Assert;
import org.junit.Test;


public class SortingAppTest {

    private final SortingApp sorting = new SortingApp();

    @Test(expected = NullPointerException.class)
    public void testNullCase() {
        int[] a = null;
        sorting.sort(a);
        Assert.assertNull(a);
    }

    @Test
    public void testEmptyCase() {
        int[] a = {};
        sorting.sort(a);
        Assert.assertArrayEquals(new int[]{}, a);

    }
}
